package main

import (
	"flag"
	"fmt"
	"os"
	"runtime"
	"strings"

	"gitlab.com/shasderias/bwenv/bw"
)

func printUsage(fs *flag.FlagSet) {
	const usage = `bwenv [OPTION...] [TERM]
bwenv uses the Bitwarden bw CLI tool to search for all secure notes containing
the string bwenv-[TERM] in its name and writes to stdout shell commands to set an
env variable for each custom field in the secure note. The name and value of
each env var are set to the name and value of each custom field.

Typical Usage
bash:
	eval$(bwenv [TERM])
fish:
	bwenv -shell fish [TERM] | source
batch:
	for /f "delims=" %i in ('bwenv [TERM]') do %i`
	fmt.Fprintf(os.Stderr, "%s\n", usage)
	fs.PrintDefaults()
}

func main() {
	var query string

	args := os.Args[1:]

	fs := flag.NewFlagSet("", flag.ExitOnError)

	fSkipSync := fs.Bool("skip-sync", false, "skip Bitwarden vault sync")
	fShell := fs.String("shell", "", "shell to output commands for (bash, fish, batch)")
	fPrefix := fs.String("prefix", "", "string to be prefixed to each env var")
	fNameMod := fs.String("name-mod", "", "modifier to be applied to the name of each env var (toUpper, toLower)")

	fs.Parse(args)

	switch fs.NArg() {
	case 0:
		// do nothing
	case 1:
		query = fs.Arg(0)
	default:
		fmt.Println(fs.Args())
		fmt.Fprintf(os.Stderr, "more than 1 search term specified, specify 0 or 1 search term\n")
		printUsage(fs)
		os.Exit(1)
	}

	if bw.Locked() {
		fmt.Fprintf(os.Stderr, "vault locked, execute bw login and/or bw unlock\n")
		os.Exit(1)
	}

	if *fSkipSync {
		fmt.Fprintf(os.Stderr, "skipping vault sync\n")
	} else {
		fmt.Fprintf(os.Stderr, "syncing vault ... ")
		if err := bw.Sync(); err != nil {
			fmt.Fprintf(os.Stderr, "error syncing bw vault\n")
			fmt.Fprintf(os.Stderr, "%v", err)
			os.Exit(1)
		}
		fmt.Fprintf(os.Stderr, "done\n")
	}



	fmt.Fprintf(os.Stderr, "finding items ... ")
	items, err := bw.SearchItems(query)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error finding items\n")
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}

	fmt.Fprintf(os.Stderr, "%d items found\n", len(items))

	var modFunc modFunc = noOpModFunc
	var env = make(envMap, 0)

	switch *fNameMod {
	case "toUpper":
		modFunc = strings.ToUpper
	case "toLower":
		modFunc = strings.ToLower
	default:
		// do nothing
	}

	for _, item := range items {
		if item.Type != bw.ItemTypeSecureNote {
			continue
		}
		for _, field := range item.Fields {
			if field.Type != bw.FieldTypeText {
				continue
			}
			name := *fPrefix + modFunc(field.Name)

			env[name] = field.Value
		}
	}

	var shell shell

	// set default shell
	switch runtime.GOOS {
	case "windows":
		shell = batchShell{}
	case "linux":
		shell = bashShell{}
	default:
		fmt.Fprintf(os.Stderr, "unsupported OS: %s\n", runtime.GOOS)
		os.Exit(1)
	}

	switch *fShell {
	case "bash":
		shell = bashShell{}
	case "fish":
		shell = fishShell{}
	case "batch":
		shell = batchShell{}
	default:
		// do nothing
	}

	shell.render(env)
}

type envMap map[string]string

type modFunc func(s string) string

func noOpModFunc(s string) string {
	return s
}

type shell interface {
	render(envMap)
}
