package bw

import (
	"encoding/json"
	"errors"
	"os"
	"os/exec"
	"time"

	"github.com/gofrs/uuid"
)

const (
	bwSessionEnvKey = "BW_SESSION"
	bwExec          = "bw"
	bwCmdSync       = "sync"
)

var (
	bwCmdListItems = []string{"list", "items", "--search"}
)

const (
	baseQuery = "bwenv"
)

type Item struct {
	Object         string        `json:"object"`
	ID             uuid.UUID     `json:"id"`
	OrganizationID uuid.NullUUID `json:"organizationId"`
	FolderID       uuid.NullUUID `json:"folderId"`
	Type           ItemType      `json:"type"`
	Name           string        `json:"name"`
	Notes          string        `json:"notes"`
	Favorite       bool          `json:"favorite"`
	Fields         []Field       `json:"fields"`
	SecureNote     SecureNote    `json:"secureNote"`
	CollectionIDs  []uuid.UUID   `json:"collectionIds"`
	RevisionDate   time.Time     `json:"revisionDate"`
}

type Field struct {
	Name  string    `json:"name"`
	Value string    `json:"value"`
	Type  FieldType `json:"type"`
}

type SecureNote struct {
	SecureNoteType `json:"secureNoteType"`
}

type ItemType int

const (
	ItemTypeLogin      = 1
	ItemTypeSecureNote = 2
	ItemTypeCard       = 3
	ItemTypeIdentity   = 4
)

type FieldType int

const (
	FieldTypeText    = 0
	FieldTypeHidden  = 1
	FieldTypeBoolean = 2
)

type SecureNoteType int

const (
	SecureNoteTypeGeneric = 0
)

func Locked() bool {
	env := os.Getenv(bwSessionEnvKey)
	if env == "" {
		return true
	}
	return false
}

func Sync() error {
	syncCmd := exec.Command(bwExec, bwCmdSync)

	return syncCmd.Run()
}

func SearchItems(query string) ([]Item, error) {
	q := baseQuery
	if query != "" {
		q += "-" + query
	}

	args := []string{}
	args = append(args, bwCmdListItems...)
	args = append(args, q)

	searchCmd := exec.Command(bwExec, args...)

	itemsJSON, err := searchCmd.Output()

	if err != nil {
		if exitErr, ok := err.(*exec.ExitError); ok {
			return nil, errors.New(string(exitErr.Stderr))
		}
		return nil, err
	}

	var items []Item

	if err := json.Unmarshal(itemsJSON, &items); err != nil {
		return nil, err
	}

	return items, nil
}
